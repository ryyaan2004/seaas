var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/"] = requestHandlers.home;
handle["off"] = requestHandlers.off;
handle["you"] = requestHandlers.you;
handle["this"] = requestHandlers.fthis;
handle["that"] = requestHandlers.fthat;
handle["everything"] = requestHandlers.everything;
handle["everyone"] = requestHandlers.everyone;
handle["donut"] = requestHandlers.donut;
handle["shakespeare"] = requestHandlers.shakespeare;
handle["linus"] = requestHandlers.linus;
handle["king"] = requestHandlers.king;
handle["pink"] = requestHandlers.pink;
handle["life"] = requestHandlers.life;
handle["chainsaw"] = requestHandlers.chainsaw;
handle["thing"] = requestHandlers.thing;
handle["thanks"] = requestHandlers.thanks;
handle["flying"] = requestHandlers.flying;

server.start(router.route, handle);