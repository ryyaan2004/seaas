  var querystring = require("querystring");
  var fs = require("fs");
  var homePage = fs.readFileSync('index.htm');

function home(response, request, homePage) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  response.end(homePage, 'utf-8');
}

function off(response, request, pathTokens) {
  return createMsg(messages.offMsg, pathTokens[3], pathTokens[2]);
}

function you(response, request, pathTokens) {
  return createMsg(messages.youMsg, pathTokens[3], pathTokens[2]);
}

function fthis(response, request, pathTokens ) {
  return createMsg(messages.thisMsg, pathTokens[2]);
}

function fthat(response, request, pathTokens ) {
  return createMsg(messages.thatMsg, pathTokens[2]);
}

function everything(response, request, pathTokens ) {
  return createMsg(messages.everythingMsg, pathTokens[2]);
}

function everyone(response, request, pathTokens ) {
  return createMsg(messages.everyoneMsg, pathTokens[2]);
}

function donut(response, request, pathTokens ) {
  return createMsg(messages.donutMsg, pathTokens[3], pathTokens[2]);
}

function shakespeare(response, request, pathTokens ) {
  return createMsg(messages.shakespeareMsg, pathTokens[3], pathTokens[2]);
}

function linus(response, request, pathTokens ) {
  return createMsg(messages.linusMsg, pathTokens[3], pathTokens[2]);
}

function king(response, request, pathTokens ) {
  return createMsg(messages.kingMsg, pathTokens[3], pathTokens[2]);
}

function pink(response, request, pathTokens ) {
  return createMsg(messages.pinkMsg, pathTokens[2]);
}

function life(response, request, pathTokens ) {
  return createMsg(messages.lifeMsg, pathTokens[2]);
}

function chainsaw(response, request, pathTokens ) {
  return createMsg(messages.chainsawMsg, pathTokens[3], pathTokens[2]);
}

function thing(response, request, pathTokens) {
  return createMsg(messages.thingMsg, pathTokens[3], pathTokens[2]);
}

function thanks(response, request, pathTokens) {
  return createMsg(messages.thanksMsg, pathTokens[3], pathTokens[2]);
}

function flying(response, request, pathTokens) {
  return createMsg(messages.flyingMsg, pathTokens[3], pathTokens[2]);
}


function createMsg(msgObj, from, to) {
  //console.log('from='+from + " to="+to);
  var msg = {};
  msg.message = msgObj.message;
  msg.subtitle = msgObj.subtitle;
  if (from && typeof from === 'string') {
    msg.subtitle = msg.subtitle.replace('$from', from )
  }
  if (to && typeof to === 'string') {
    msg.message = msg.message.replace('$to', to);
  }
  return msg;
}
 
var messages = {};
var offMsg = {};
offMsg.message="Fuck off, $to";
offMsg.subtitle="- $from";
messages.offMsg = offMsg;

var youMsg = {}
youMsg.message="Fuck you, $to";
youMsg.subtitle="- $from";
messages.youMsg = youMsg;

var thisMsg = {}
thisMsg.message="Fuck this.";
thisMsg.subtitle="- $from";
messages.thisMsg = thisMsg;

var thatMsg = {}
thatMsg.message="Fuck that.";
thatMsg.subtitle="- $from";
messages.thatMsg = thatMsg;

var everythingMsg = {}
everythingMsg.message="Fuck everything.";
everythingMsg.subtitle="- $from";
messages.everythingMsg = everythingMsg;

var everyoneMsg = {}
everyoneMsg.message="Fuck everything.";
everyoneMsg.subtitle="- $from";
messages.everyoneMsg = everyoneMsg;

var donutMsg = {}
donutMsg.message="$to, go and take a flying fuck at a rolling donut.";
donutMsg.subtitle="- $from";
messages.donutMsg = donutMsg;

var shakespeareMsg = {}
shakespeareMsg.message="$to, Thou clay-brained guts, thou knotty-pated fool, thou whoreson obscene greasy tallow-catch!";
shakespeareMsg.subtitle="- $from";
messages.shakespeareMsg = shakespeareMsg;

var linusMsg = {}
linusMsg.message="$to, there aren't enough swear-words in the English language, so now I'll have to call you perkeleen vittupää just to express my disgust and frustration with this crap.";
linusMsg.subtitle="- $from";
messages.linusMsg = linusMsg;

var kingMsg = {}
kingMsg.message="Oh fuck off, just really fuck off you total dickface. Christ $to, you are fucking thick.";
kingMsg.subtitle="- $from";
messages.kingMsg = kingMsg;

var pinkMsg = {}
pinkMsg.message="Well, Fuck me pink.";
pinkMsg.subtitle="- $from";
messages.pinkMsg = pinkMsg;

var lifeMsg = {}
lifeMsg.message="Fuck my life.";
lifeMsg.subtitle="- $from";
messages.lifeMsg = lifeMsg;

var chainsawMsg = {}
chainsawMsg.message="Fuck me gently with a chainsaw, $to. Do I look like Mother Teresa? ";
chainsawMsg.subtitle="- $from";
messages.chainsawMsg = chainsawMsg;

var thingMsg = {}
thingMsg.message="Fuck my life.";
thingMsg.subtitle="- $from";
messages.thingMsg = thingMsg;

var thanksMsg = {}
thanksMsg.message="Fuck you very much.";
thanksMsg.subtitle="- $from";
messages.thanksMsg = thanksMsg;

var flyingMsg = {}
flyingMsg.message="I don't give a flying fuck.";
flyingMsg.subtitle="- $from";
messages.flyingMsg = flyingMsg;



exports.home = home;
exports.off = off;
exports.you = you;
exports.fthis = fthis;
exports.fthat = fthat;
exports.everything = everything;
exports.everyone = everyone;
exports.donut = donut;
exports.shakespeare = shakespeare;
exports.linus = linus;
exports.king = king;
exports.pink = pink;
exports.life = life;
exports.chainsaw = chainsaw;
exports.thing = thing;
exports.thanks = thanks;
exports.flying = flying;