var http = require("http");
var url = require("url");
var homePage = require('fs').readFileSync('index.htm');

function start(route, handle) {
  function onRequest(request, response) {
  	// might want to wrap request.url with decodeURI()
    var pathname = url.parse(request.url).pathname;
    console.log("Request for " + pathname + " received.");
    route(handle, pathname, response, request, homePage);
  }

  http.createServer(onRequest).listen(8888);
  console.log("Server has started.");
}

exports.start = start;
