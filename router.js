function route(handle, pathname, response, request, homePage) {
  console.log("About to route a request for " + pathname);

  if (pathname === "/favicon.ico") {
    response.end();
    return;
  }

  if (pathname === '' || pathname === '/' || pathname === '/index.htm') {
    console.log("handling home page...");
    handle['/'](response, request, homePage);
    return;
  }

  var parsePathTokens = /\/([A-Za-z]*)\/([A-Za-z]*)(?:\/([A-Za-z]*))?/;  
  var pathTokens = parsePathTokens.exec(pathname);

  if (!pathTokens || pathTokens.length < 2) {
    console.log("sending to home page...");
    handle['/'](response, request);
  } else if (typeof handle[pathTokens[1]] === 'function') {
  	var msg = handle[pathTokens[1]](response, request, pathTokens);
    contentNegotiation(request, response, msg);
  } else {
    console.log("No request handler found for " + pathTokens[1]);
    response.writeHead(404, {"Content-Type": "text/html"});
    response.write("404 Not found");
    response.end();
  }

}

function contentNegotiation(request, response, msg) {
  var accept = request.headers.accept.split(',');
  if (accept[0] === 'text/plain') {
    response.writeHead(200, {"Content-Type": "text/plain"});
    var body = msg.message+"\n"+msg.subtitle;
    response.write(body);
    response.end();

  } else if (accept[0] === 'text/html') {
    response.writeHead(200, {"Content-Type": "text/html"});
    var body = "<html><head><link href=\"//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css\" rel=\"stylesheet\"></head>"+
    "<body><div class=\"hero-unit\">"+
    "<h1>"+msg.message+"</h1>"+
    "<h2>"+msg.subtitle+"</h2>"+
    "</div></body></html>";
    response.write(body);
    response.end();

  } else if (accept[0] === 'application/json') {
    response.writeHead(200, {"Content-Type": "application/json"});
    var body = {};
    body.message = msg.message;
    body.subtitle = msg.subtitle;
    response.write(JSON.stringify(body));
    response.end();
  } else {

    console.log("No acceptable Accept header type found");
    response.writeHead(404, {"Content-Type": "text/html"});
    response.write("404 Not found");
    response.end();
  }

}


exports.route = route;